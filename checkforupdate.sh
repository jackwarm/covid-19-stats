#!/bin/bash 
date
TODAY=$(date --date='12:00 yesterday' +"%m-%d-%Y");
FILE="$TODAY.csv"
GET="wget https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_daily_reports/$FILE"
PACKAGE_DIR="$HOME/covid-19-stats"
UPDATE_FILE="$PACKAGE_DIR/last_update"
DONE=$(grep $TODAY $UPDATE_FILE)

$GET >/dev/null 2>/dev/null
if [[ -e $FILE && -z "$DONE" ]]
then
	echo "Running Pipleline for $FILE"
	cd $PACKAGE_DIR
	git pull
	echo $TODAY > $UPDATE_FILE
	git add $UPDATE_FILE
	git commit -m "$TODAY update"
	git push
else
	echo "No Update for $FILE"
fi
cd ~
if [ -e $FILE ]
then
	rm $FILE
fi
echo ""
