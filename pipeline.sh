#!/bin/bash -x
#
# Nodus 4.0
#jobOutput=`sudo /usr/local/bin/runNodusJob $PWD ray-demo covid-job.json ray-demo`
#echo $jobOutput 
#
# Nodus 4.1
CLUSTER='otc-slurm-core-singularity'
USER='test51'
JOBNAME='covid'
SCRIPT="$PWD/corona.sh"
FILE1="$PWD/input/process_covid.pl"
FILE2="$PWD/input/regions.txt"
CORES=1
NODES=1

# Run the Job
sudo /usr/local/bin/runJobAndWait.sh --user $USER --name $JOBNAME --cores $CORES --nodes $NODES --cluster $CLUSTER \
	--script $SCRIPT --file $FILE1 --file $FILE2

#
# Verify the results
#
exit 0
