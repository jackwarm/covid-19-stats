#!/usr/bin/perl
use Text::ParseWords;
use strict;
use warnings;

# process command lines
my $DEBUG = 0;
my $showAll = 1;
my $showRegions = 0;
my $showStates = 0;
my %showCountries;
my $processShowCountries=0;
my $showCounties = 0;
my $showCountriesField = "confirmed";
my $outputDirectory = "../output/";
for (my $i=0; $i < @ARGV; $i++) {
	if ($ARGV[$i] eq '-d') {
		$showAll = 0;	
	} elsif ($ARGV[$i] eq '-r') {
		$showRegions = 1;	
	} elsif ($ARGV[$i] eq '-c') {
		my $file = $ARGV[$i+1];
		open(FH, '<', $file) or die $!;
        	while(<FH>){
			chomp($_);
			$_ =~ s/_/ /g;
			$showCountries{$_} = 0;
		}
		$processShowCountries=1;
		close FH
	} elsif ($ARGV[$i] eq '-cm') {
		$showCountriesField = $ARGV[$i+1];
	} elsif ($ARGV[$i] eq '-a') {
		$showStates = 1;	
		$showCounties = 1;
	} elsif ($ARGV[$i] eq '-s') {
		$showStates = 1;
	} elsif ($ARGV[$i] eq '-o') {
		$outputDirectory = $ARGV[$i+1];
	}
}

# initalize variables
my $directory = './COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/';
my @days;
opendir (DIR, $directory) or die "$!: $directory";
while (my $file = readdir(DIR)) {
	next if ($file =~ m/^\./);
	next if ($file =~ m/^READ/);
	push @days,$file;
}
@days = sort @days;

# read each days file
my %lastDay;
my %lastDayStates;
my %allStates;
my %lastTotal = (confirmed=>0,deaths=>0,recovered=>0,confirmed_new=>0,deaths_new=>0,recovered_new=>0);

printf ("%30s %10s%s  %10s %s  %10s          %s\n","Date/Region","Confirmed","(Change)","Deaths","(Change)","Recovered","ROD");
open (C, ">", "$outputDirectory/daily.csv");
print C "Date,Confirmed,Deaths,Recovered,Active,Total Cases,Total Deaths,Total Recover\n";

if (scalar %showCountries) {
	open (L, ">", "$outputDirectory/regions.csv");
	print L "Date";
	for my $region ( sort keys %showCountries) { 
		print L ",". $region;
	}
	print L "\n";
}
my $process = 0;
for (my $i=0; $i <scalar @days; $i++) {
	my %todayData;
	my %statesData;
	my %statesList;
	my($date) = split(/\./,$days[$i]);
	if ($DEBUG) {
	    $process = 1 if ($date eq "03-22-2020");
	    next if (!$process);
	}
	my $filename = $directory . $days[$i];
	open(FH, '<', $filename) or die $!;
	my $first = 1;

	# read the contents of the files
	my $newFormat = 0;
	while(<FH>){
		my ($FIPS,$admin2,$state,$region,$lastUpdate,$lat,$long,$confirmed,$deaths,$recovered);
		if ($first) {
			$first = 0;
			$newFormat = 1 if ($_ =~ /FIPS/);
			print "$newFormat: $_" if ($DEBUG);
			next;
		}
		my $row = $_;
		chop($row);
		chop($row);
		$row =~ s/\'//g;
		$row =~ s/^\s//;
		$row =~ s/.Korea, South./Korea South/g;
		if ($newFormat) {
			$row =~ s/\"//g;
			my @csv = quotewords ',', 0 ,$row;
			($FIPS,$admin2,$state,$region,$lastUpdate,$lat,$long,$confirmed,$deaths,$recovered) = @csv;
			if ($admin2 && $showCounties) {
				$state = $region . "_" . $state . "_" . $admin2;
			} else {
				$state = $region . "_" . $state;
			}
		} else {
			my @csv = quotewords ',', 0 ,$row;
			($state,$region,$lastUpdate,$confirmed,$deaths,$recovered) = @csv;
		}
		print "($state,$region,$lastUpdate,$confirmed,$deaths,$recovered)\n" if ($DEBUG);

		$region =~ s/\,//g;
		$region =~ s/\*//g;
		$region =~ s/ SAR//g;
		$region = "Macau" if ($region =~ /^Macao/i);
		$region = "United_Kingdom" if ($region =~ /^UK/i);
		$region = "Russia" if ($region =~ /^Russian Federation/i);
		$region = "Azerbaijan" if ($region =~ /^.Azerbaijan/i);
		$region = "Saint Martin" if ($region =~ /^St. Martin/i);
		$region = "Vatican City" if ($region =~ /^Holy See/i);
		$region = "Vietnam" if ($region =~ /^Viet Nam/i);
		$region = "Congo" if ($region =~ /^Congo/i);
		$region = "Iran" if ($region =~ /^Iran/i);
		$region = "China" if ($region =~ "Mainland China");
		$region = "South Korea" if ($region =~ "Korea South");
		$region = "South Korea" if ($region =~ "Republic of Korea");
		$confirmed = 0 if ((!defined($confirmed)) || length($confirmed) == 0);
		$deaths = 0 if ((!defined($deaths)) || length($deaths) == 0);
		$recovered = 0 if ((!defined($recovered)) || length($recovered) == 0);
		if (exists $todayData{$region}) {
			$todayData{$region}{confirmed} += $confirmed;
			$todayData{$region}{deaths} += $deaths;
			$todayData{$region}{recovered} += $recovered;
		} else {
			$todayData{$region} = {confirmed=>int($confirmed),deaths=>int($deaths),recovered=>int($recovered),
					       confirmed_new=>0,deaths_new=>0,recovered_new=>0};
			$todayData{$region}{increase} = sprintf ("%9s"," ");
			$todayData{$region}{dincrease} = sprintf ("%9s"," ");
		}

		# Save States Data if requested
		if ($newFormat && $showStates) {
                	if (exists $statesData{$state}) {
                        	$statesData{$state}{confirmed} += $confirmed;
                        	$statesData{$state}{deaths} += $deaths;
                        	$statesData{$state}{recovered} += $recovered;
                	} else {
                        	$statesData{$state} = {confirmed=>int($confirmed),deaths=>int($deaths),recovered=>int($recovered)};
				$statesData{$state}{increase} = sprintf ("%9s"," ");
                	}

		}
		if (!exists $statesList{$state}) {
			$statesList{$state} = 1;
		}
	}
	close(FH);
	
	# Compare with Previous Day
	for my $region ( sort keys %todayData) { 
		if (exists $lastDay{$region}) {
			$todayData{$region}{increase} = calcIncrease($todayData{$region}{confirmed},$lastDay{$region}{confirmed});
			$todayData{$region}{dincrease} = calcIncrease($todayData{$region}{deaths},$lastDay{$region}{deaths});
			$todayData{$region}{confirmed_new} = $todayData{$region}{confirmed} - $lastDay{$region}{confirmed};
			$todayData{$region}{deaths_new} = $todayData{$region}{deaths} - $lastDay{$region}{deaths};
			$todayData{$region}{recovered_new} = $todayData{$region}{recovered} - $lastDay{$region}{recovered};
		}
	}
	if ($showStates) {
		for my $state ( sort keys %statesData) { 
			if (exists $lastDayStates{$state}) {
				$statesData{$state}{increase} = calcIncrease($statesData{$state}{confirmed},$lastDayStates{$state}{confirmed});
			    $statesData{$state}{dincrease} = calcIncrease($statesData{$state}{deaths},$lastDayStates{$state}{deaths});
		    	$statesData{$state}{confirmed_new} = $statesData{$state}{confirmed} - $lastDayStates{$state}{confirmed};
		    	$statesData{$state}{deaths_new} = $statesData{$state}{deaths} - $lastDayStates{$state}{deaths};
		    	$statesData{$state}{recovered_new} = $statesData{$state}{recovered} - $lastDayStates{$state}{recovered};
			}
		}
	}
	
	# print day results
	my %todayTotal = (confirmed=>0,deaths=>0,recovered=>0,confirmed_new=>0,deaths_new=>0,recovered_new=>0);
	for my $region ( sort keys %todayData) { 
		$todayTotal{confirmed} += $todayData{$region}{confirmed};
		$todayTotal{deaths}  += $todayData{$region}{deaths};
		$todayTotal{recovered}+= $todayData{$region}{recovered};
		$todayTotal{confirmed_new}+= $todayData{$region}{confirmed_new};
		$todayTotal{deaths_new}+= $todayData{$region}{deaths_new};
		$todayTotal{recovered_new}+= $todayData{$region}{recovered_new};
		my $dpct =  calcPct($todayData{$region}{deaths},$todayData{$region}{confirmed});
		printf ("%30s",$region) if ($showAll);
		printf (" %10d",$todayData{$region}{confirmed}) if ($showAll);
		printf ("%9s",$todayData{$region}{increase}) if ($showAll);
		printf (" %10d",$todayData{$region}{deaths}) if ($showAll);
		printf ("%9s",$todayData{$region}{dincrease}) if ($showAll);
		printf (" %10d",$todayData{$region}{recovered}) if ($showAll);
		printf (" (%4.2f%%)", $dpct) if ($showAll);
		print "\n" if ($showAll);
		if ($showRegions) {
            printRegions($date,$region,%todayData);
            printRegionsCSV($date,$region,%todayData);
		}
	}
	if ($showStates) {
		for my $state ( sort keys %statesData) { 
			printStates($date,$state,%statesData);
			printStatesCSV($date,$state,%statesData) if (defined($statesData{$state}));
		}
	}

	# Caclulate previous increase;
	my $showTotalPct = calcIncrease($todayTotal{confirmed},$lastTotal{confirmed});
	my $showDeathPct = calcIncrease($todayTotal{deaths},$lastTotal{deaths});
	%lastTotal = %todayTotal;
	%allStates = %statesList;
	%lastDay = %todayData;
	%lastDayStates = %statesData;

	# print totals
	if ($processShowCountries > 0) {
		print L "$date";
		for my $region ( sort keys %showCountries) { 
			if (defined($todayData{$region}{$showCountriesField})) {
				print L ",". $todayData{$region}{$showCountriesField};
			} else {
				print L ",0";
			}
		}
		print L "\n";
	}
	print "------------------------------\n" if ($showAll);
	printf ("%30s %10d%s %10d%s %10d         %4.2f%%\n",$date,$todayTotal{confirmed},$showTotalPct,$todayTotal{deaths},$showDeathPct,$todayTotal{recovered},calcPct($todayTotal{deaths},$todayTotal{confirmed}));
	print "------------------------------\n\n" if ($showAll);
	printf C "%s, %d, %d, %d, %d, %d, %d, %d\n",$date,$todayTotal{confirmed_new},$todayTotal{deaths_new},$todayTotal{recovered_new},
		($todayTotal{confirmed}-($todayTotal{deaths}+$todayTotal{recovered})),
		$todayTotal{confirmed},$todayTotal{deaths},$todayTotal{recovered};
}
close (C);
close (L);
dumpRegions(%lastDay) if ($showRegions);
dumpStates(%allStates) if ($showStates);
exit(0);

#---------------------------------------------------------------------------------------------------------
#
sub printStates {
	my $dir = "$outputDirectory/states/";
	my ($date,$state,%data) = @_;

	my $file = $state;
	$file =~ s/ /_/g;
	mkdir $dir if (! -d $dir) ;
	open (FH, ">>", $dir.$file.".txt");
	printf FH "%30s %10d%s %10d (%4.2f%%) %10d\n",$date,$data{$state}{confirmed},$data{$state}{increase},$data{$state}{deaths},calcPct($data{$state}{deaths},$data{$state}{confirmed}),$data{$state}{recovered};
	close FH;
}

sub printStatesCSV {
	my $dir = "$outputDirectory/states/";
	my ($date,$state,%data) = @_;

	my $file = $state;
	$file =~ s/ /_/g;
	mkdir $dir if (! -d $dir) ;
	open (FH, ">>", $dir.$file.".csv");
	printf FH "%s, %d, %d, %d\n",$date,$data{$state}{confirmed_new},$data{$state}{deaths_new},$data{$state}{recovered_new} if (defined($data{$state}{confirmed_new}));
	close FH;
}

sub printRegions {
	my $dir = "$outputDirectory/regions/";
	my ($date,$region,%data) = @_;

	my $file = $region;
	$file =~ s/ /_/g;
	mkdir $dir if (! -d $dir) ;
	open (FH, ">>", $dir.$file.".txt");
	printf FH "%30s %10d%s %10d%s %10d       (%4.2f%%)\n",$date,$data{$region}{confirmed},$data{$region}{increase},$data{$region}{deaths},$data{$region}{dincrease},$data{$region}{recovered},calcPct($data{$region}{deaths},$data{$region}{confirmed});
	close FH;
}

sub printRegionsCSV {
	my $dir = "$outputDirectory/regions/";
	my ($date,$region,%data) = @_;

	my $file = $region;
	$file =~ s/ /_/g;
	mkdir $dir if (! -d $dir) ;
	open (FH, ">>", $dir.$file.".csv");
	printf FH "%s, %d, %d, %d\n",$date,$data{$region}{confirmed_new},$data{$region}{deaths_new},$data{$region}{recovered_new};
	close FH;
}


sub dumpStates {
	my %data = @_;
	open (FH, ">", "$outputDirectory/states.txt");
        for my $state ( sort keys %data) {
		print FH "$state\n" if (length($state) > 0);
	}
	close(FH) or "Couldn't close the file";
}

sub dumpRegions {
	my %data = @_;
	open (FH, ">", "$outputDirectory/regions.txt");
        for my $region ( sort keys %data) {
		print FH "$region\n" if (length($region) > 0);
	}
	close(FH) or "Couldn't close the file";
}

sub calcIncrease {
	my ($today,$yesterday) = @_;
	my $result = sprintf ("%9s"," ");
        if ($yesterday > 0) {
                my $increase = $today - $yesterday;
                $result = sprintf("(+%5.2f%%)", calcPct($increase,$yesterday));
        }
	return $result;
}

sub calcPct {
	my ($count, $total) = @_;
	my $value = 0;
	if ($total > 0) {
		$value = ($count/$total) * 100.0;
	}
	return $value;
}
