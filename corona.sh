#!/bin/bash -x
date
#
OUTPUT_DIR="../output"
if [ ! -z "$PBS_O_HOST" ]
then
  PBS_JOBID="${PBS_JOBID/$PBS_O_HOST/$PBS_JOBNAME}"
  OUTPUT_DIR="/NODUS/share/$PBS_JOBID"
fi
cd input
chmod +x process_covid.pl
mkdir $OUTPUT_DIR
rm -rf $OUTPUT_DIR/regions
rm -rf $OUTPUT_DIR/states
rm -rf x COVID-19/
git clone https://github.com/CSSEGISandData/COVID-19.git

# Process the repository
COUNT=0
./process_covid.pl -r -o $OUTPUT_DIR >$OUTPUT_DIR/total.txt &
pids[$COUNT]=$!
COUNT=$(($COUNT+1))

./process_covid.pl -s -o $OUTPUT_DIR >/dev/null &
pids[$COUNT]=$!
COUNT=$(($COUNT+1))

`./process_covid.pl -d -c regions.txt -o $OUTPUT_DIR >$OUTPUT_DIR/daily.txt ; mv $OUTPUT_DIR/regions.csv $OUTPUT_DIR/regions_deaths.csv ` &
pids[$COUNT]=$!
COUNT=$(($COUNT+1))

./process_covid.pl -c regions.txt -cm deaths -o $OUTPUT_DIR >/dev/null &
pids[$COUNT]=$!
COUNT=$(($COUNT+1))

# This is the longest Running Function
#./process_covid.pl -a -o $OUTPUT_DIR >/dev/null &
#pids[$COUNT]=$!
#COUNT=$(($COUNT+1))

# Wait for the jobs to finish
# wait for all pids
for pid in ${pids[*]}; do
    wait $pid
done
cat $OUTPUT_DIR/daily.txt
date
